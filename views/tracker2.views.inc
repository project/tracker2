<?php

/**
 * @file
 * Provides support for Views integration.
 */

/**
 * Implementation of hook_views_data().
 */
function tracker2_views_data() {
  $data = array();

  $data['tracker2_node']['table']['group']  = t('Tracker 2');
  $data['tracker2_node']['table']['join'] = array(
    'node' => array(
      'type' => 'INNER',
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );
  $data['tracker2_node']['nid'] = array(
    'title' => t('NID'),
    'help' => t('The node ID of the node.'),
    'field' => array(
      'handler' => 'views_handler_field_node',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_node_nid',
      'parent' => 'views_handler_argument_numeric', // make sure parent is included
      'name field' => 'title', // the field to display in the summary.
      'numeric' => TRUE,
      'validate type' => 'nid',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['tracker2_node']['changed'] = array(
    'title' => t('Updated date'),
    'help' => t('The date the node was last updated.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
  $data['tracker2_node']['published'] = array(
    'title' => t('Published'),
    'help' => t('Whether or not the node is published.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'tracker2_handler_filter_boolean_operator',
      'label' => t('Published'),
      'type' => 'yes-no',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['tracker2_user']['table']['group']  = t('Tracker 2 - User');
  $data['tracker2_user']['table']['join'] = array(
    'node' => array(
      'type' => 'INNER',
      'left_field' => 'nid',
      'field' => 'nid',
    ),
    'user' => array(
      'type' => 'INNER',
      'left_field' => 'uid',
      'field' => 'uid',
    ),
  );
  $data['tracker2_user']['nid'] = array(
    'title' => t('NID'),
    'help' => t('The node ID of the node a user created or commented on. You must use an argument or filter on UID or you will get misleading results using this field.'),
    'field' => array(
      'handler' => 'views_handler_field_node',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_node_nid',
      'parent' => 'views_handler_argument_numeric', // make sure parent is included
      'name field' => 'title', // the field to display in the summary.
      'numeric' => TRUE,
      'validate type' => 'nid',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['tracker2_user']['uid'] = array(
    'title' => t('UID'),
    'help' => t('The user ID of a user who touched the node (either created or commented on it).'),
    'field' => array(
      'handler' => 'views_handler_field_user',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_user_uid',
      'name field' => 'name', // display this field in the summary
    ),
    'filter' => array(
      'title' => t('Name'),
      'handler' => 'views_handler_filter_user_name',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['tracker2_user']['changed'] = array(
    'title' => t('Updated date'),
    'help' => t('The date the node was last updated or commented on. You must use an argument or filter on UID or you will get misleading results using this field.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
  $data['tracker2_user']['published'] = array(
    'title' => t('Published'),
    'help' => t('Whether or not the node is published. You must use an argument or filter on UID or you will get misleading results using this field.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'tracker2_handler_filter_boolean_operator',
      'label' => t('Published'),
      'type' => 'yes-no',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  return $data;
}

/**
 * Implementation of hook_views_data_alter().
 */
function tracker2_views_data_alter(&$data) {
  // Swap out the handlers for the uid_touch filter and argument.
  $data['node']['uid_touch']['argument']['handler'] = 'tracker2_handler_argument_comment_user_uid';
  $data['node']['uid_touch']['filter']['handler'] = 'tracker2_handler_filter_comment_user_uid';
}

/**
 * Implementation of hook_views_handlers().
 */
function tracker2_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'tracker2') . '/views',
    ),
    'handlers' => array(
      'tracker2_handler_argument_comment_user_uid' => array(
        'parent' => 'views_handler_argument_comment_user_uid',
      ),
      'tracker2_handler_filter_boolean_operator' => array(
        'parent' => 'views_handler_filter_boolean_operator',
      ),
      'tracker2_handler_filter_comment_user_uid' => array(
        'parent' => 'views_handler_filter_comment_user_uid',
      ),
    ),
  );
}
