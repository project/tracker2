<?php

/**
 * UID argument to check for nodes that user posted or commented on.
 */
class tracker2_handler_argument_comment_user_uid extends views_handler_argument_comment_user_uid {
  function query() {
    // Because this handler thinks it's an argument for a field on the {node}
    // table, we need to make sure {tracker2_user} is JOINed and use its alias
    // for the WHERE clause.
    $tracker2_user_alias = $this->query->ensure_table('tracker2_user');
    $this->query->add_where(0, "$tracker2_user_alias.uid = %d", $this->argument);
  }
}
